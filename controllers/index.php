<?php

namespace package_name\controllers;

use \mako\View;

class Index extends \mako\Controller
{
	public function action_index()
	{
		return new View('package_name::welcome');
	}
}