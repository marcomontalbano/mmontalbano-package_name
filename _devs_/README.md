_\_devs\__ folder
=================

This _**\_devs\_**_ folder contains 2 other folders that are:

* **demo**: used to test and present a demo of all functionalities.
* **tests**: test driven development is ”clean code that works”.

##demo##

You can use this folder to install a new mako application and use it to test your own package.

First of all you need to create a new mako application.

```sh
php composer.phar create-project mako/app demo

```

Link your package into packages folder of the mako application.

```sh
cd demo/app/packages/
ln -s ../../../../ package_name

```

Read Mako Framework documentation to [configure](http://makoframework.com/docs/3.6/packages) it.

Now you can start to use and test your package.

```text
./mmontalbano-package_name/
├── _devs_
│   ├── demo
│   │   ├── app
│   │   │   ├── ..
│   │   │   ├── packages
│   │   │   │   └── package_name -> ../../../../
│   │   │   └── ..
│   │   └── ..
│   ├── tests
│   └── README.md
├── ..
├── .gitignore
├── _init.php
├── composer.json
└── README.md

```

##tests##

[Codeception] is a multi-featured testing framework for PHP. It can handle unit, functional, and acceptance testing of web applications and it’s powered by the already very popular PHPUnit testing framework.

Codeception allows us to test different kinds of user perspectives and site scenarios while they are visiting our app to ensure a pleasant user experience. By testing multiple scenarios, we can simulate a user’s natural flow throughout our application to make sure the app is working as expecting.

```sh
# move to '_devs_' folder
cd _devs_/

# install codeception
php ../vendor/bin/codecept bootstrap

# write your first test
php codecept.phar generate:cept unit "Welcome"
php codecept.phar generate:cept unit "namespace\Welcome"

```

[Quick Start!] Write and execute a test for an existing app in less then 5 mins! No additional tools required.


----------------

Copyright © 2013-2014. Marco Montalbano. All rights reserved.

[Codeception]:http://codeception.com/
[Quick Start!]:http://codeception.com/quickstart
