MarkSite | Package Name
=======================

Demonstrative mako framework package to set up a project. My skeleton :)

##Start from scratch##

Create the following folder structure:

```text
./mmontalbano-package_name/
├── _devs_
│   ├── demo
│   ├── tests
│   └── README.md
├── .gitignore
├── composer.json
└── README.md

```

###.gitignore###

A [.gitignore] file specifies intentionally untracked files that Git should ignore. Files already tracked by Git are not affected.

You can start to populate your _.gitignore_ customizing the following:

```text
.DS_Store
Thumbs.db
composer.phar
composer.lock
/vendor

```

###composer.json###

A [composer.json] file describes the dependencies of your project and may contain other metadata as well.

You can start to populate your _composer.json_ customizing the following:

```json
{
    "name": "mmontalbano/package_name",
    "type": "mako-package",
    "description": "Demonstrative mako framework package to set up a project. My skeleton :)",
    "homepage": "https://bitbucket.org/mmontalbano/mmontalbano-package_name",
    "authors": [
        {
            "name"  : "Marco Montalbano",
            "email" : "marco.montalbano@marksite.it"
        }
    ],
    "keywords": [
        "mmontalbano",
        "mako",
        "framework",
        "package",
        "skeleton"
    ],
    "license": "BSD-3-Clause",
    "support": {
        "wiki"   : "https://bitbucket.org/mmontalbano/mmontalbano-package_name/wiki",
        "issues" : "https://bitbucket.org/mmontalbano/mmontalbano-package_name/issues"
    },
    "require": {
        "php": ">=5.4.0",
        "composer/installers": "*"
    },
    "require-dev": {
        "codeception/codeception": "*"
    },
    "config": {
        "preferred-install": "dist"
    },
    "minimum-stability": "dev"
}

```

After it, run:

```sh
php composer.phar install

```

###README.md###

[Markdown] is a text-to-HTML conversion tool for web writers. Markdown allows you to write using an easy-to-read, easy-to-write plain text format, then convert it to structurally valid XHTML (or HTML).

Check out the [syntax] to start to write your project documentation.

###Write out your package###

Now you can start to write out your personal package adding folders, scripts, assets directly inside the main folder:

```text
./mmontalbano-package_name/
├── _devs_
│   ├── demo
│   ├── tests
│   └── README.md
├── config
│   └── routes.php
├── controllers
│   └── index.php
├── views
│   └── welcome.php
├── .gitignore
├── _init.php
├── composer.json
└── README.md

```

###What is the _\_devs\__ folder?###

Check out this [_README.md_](/mmontalbano/mmontalbano-package_name/src/master/_devs_/) file for more details.

##How to use it##

[Packagist](https://packagist.org/) is the main [Composer](http://getcomposer.org/) repository. It aggregates all sorts of PHP packages that are installable with Composer.

###Define Your Dependencies###

Put a file named _composer.json_ at the root of your project, containing your project dependencies:

```json
{
    "require": {
        "mmontalbano/package_name": "*"
    }
}

```

###Install Composer In Your Project###

Run this in your command line:

```sh
curl -s http://getcomposer.org/installer | php

```

###Install Dependencies###

Execute this in your project root.

```sh
php composer.phar install

```


----------------

Copyright © 2013-2014. Marco Montalbano. All rights reserved.

[.gitignore]:http://git-scm.com/docs/gitignore
[composer.json]:http://getcomposer.org/doc/01-basic-usage.md
[markdown]:http://daringfireball.net/projects/markdown/
[syntax]:http://daringfireball.net/projects/markdown/syntax
