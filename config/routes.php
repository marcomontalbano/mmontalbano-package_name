<?php

//---------------------------------------------
// Routing configuration
//---------------------------------------------

return array
(
	/**
	 * Default route.
	 */

	'default_route' => 'index/index',
);

/** -------------------- End of file --------------------**/